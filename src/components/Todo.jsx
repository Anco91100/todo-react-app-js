import { Button, Form } from "react-bootstrap";
import React from "react";
import { connect } from "react-redux";
import { addTodo, removeTodo, updatTodo } from "../actions/todoActions";
import TodoList from "./TodoList";



class Todo extends React.Component{
    /*constructor(props){
        super(props);
        this.state={
            items:[],
            text:''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
    }
    handleSubmit(e){
        e.preventDefault();
        if(this.state.text.length ===0){
            return;
        }
        const newItem = {
            text : this.state.text,
            id: Date.now()
        }
        this.setState({
            items: this.state.items.concat(newItem),
            text: ''
        });
    }

    handleDelete(index){
        console.log('hey', index)
        const newItems = [...this.state.items];
        newItems.splice(index,1);
        this.setState({
            items: newItems
        })
    }

    handleUpdate(i){
        const newItems = [...this.state.items];
        newItems.map((item,index)=>{
            if(index === i)
            {
                item.text = this.state.text;
                return item;
            }
            else {
                return item;
            }
        });
        this.setState({
            items: newItems,
            text:''
        })
    }
*/

constructor(props){
    super(props);
    this.state = {
        text:''
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
}

handleChange(e){
    this.setState({
        text: e.target.value
    })
}

handleUpdate(){
    this.setState({
        text:''
    })
}
    render(){
        console.log(this.props);
        return(
            <div>
            <Form onSubmit={(e)=> {
                e.preventDefault();
                this.props.addTodo(this.state.text)
                this.setState({
                    text: ''
                });
            }}>
                <Form.Group className="mb-1" controlId="formBasicText">
                <Form.Control
                value={this.state.text}
                onChange={this.handleChange}
                type="text"
                placeholder="Enter todo"
                />
                <Button variant="outline-success" type="submit">
                    Ajouter 
                </Button>
                </Form.Group>
            </Form>
            {this.props.data &&
            this.props.data.todo.map((item)=>{
                let content = item.content.trim()
                return content.length ? <TodoList text={content} updaterTrigger={this.handleUpdate} updateText={content} key={item.index} index={item.index} onUpdate={this.props.updateTodo} onDelete={this.props.removeTodo} /> : false
            })
            }
         </div> 
        )
               
    }
}

const mapDispatchToProps = (dispatch) =>{
    return {
        addTodo: (text) => dispatch(addTodo(text)),
        updateTodo: (id,text) => dispatch(updatTodo(id,text)),
        removeTodo: (id) => dispatch(removeTodo(id))
    }
};

const mapStateToProps = state => ({
    data: state
});


export default connect(mapStateToProps,mapDispatchToProps)(Todo)