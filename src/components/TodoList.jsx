import { Button } from "react-bootstrap";
import React from "react";
import { Card } from "react-bootstrap";

class TodoList extends React.Component{
    render(){
        return(
            <div>
                <Card>
                {this.props.text}
                <span>
                    <Button variant="outline-danger"onClick={() => this.props.onDelete(this.props.index)}> DELETE</Button>
                    <Button variant="outline-primary" onClick={() => {this.props.updaterTrigger(); if(this.props.updateText.length>0) this.props.onUpdate(this.props.index,this.props.updateText)}}> UPDATE </Button>
                </span>
                </Card>
            </div>
        )
    }
}

export default TodoList;