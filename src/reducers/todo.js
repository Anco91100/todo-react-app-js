import { ADD_TODO, UPDATE_TODO, REMOVE_TODO } from "../actions/type";
const initialState = []

const todos = (state = initialState, action) =>{
    switch(action.type){
        case ADD_TODO: {
            return state.concat({...action.payload})
          }
        case UPDATE_TODO: {
            return state.map((item)=>{
                if(item.index === action.payload.index){
                    item.content = action.payload.content
                    return item;
                }
                else {
                    return item;
                }
            })
        }
        case REMOVE_TODO: {
            return state.filter((item)=>{
                console.log(item)
                console.log(action.payload)
                console.log(item.index);
                console.log(action.payload.index);
                if(item.index !== action.payload.index){
                    return item;
                }
            })
        }
        default:
            return state
    }
};

export default todos;