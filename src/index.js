import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './reducers/index';

const store = createStore(reducer)
console.log(store.getState());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

/*store.dispatch({
  type: 'ADD_TODO',
  payload:{
    index: 1,
    content: 'Hey c est grady'
  }
}
);
store.dispatch({
  type: 'ADD_TODO',
  payload:{
    index: 2,
    content: 'Hey c est grady2'
  }
});
*/
console.log(store.getState());

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
