import {ADD_TODO,UPDATE_TODO,REMOVE_TODO} from './type';

let index = 0;
export const addTodo= (text) => ({
    type: ADD_TODO,
    payload:{
        index: index++,
        content: text
    }
});

export const updatTodo = (id,text) => ({
    type: UPDATE_TODO,
    payload: {
        index: id,
        content:text
    }
})

export const removeTodo = (id) => ({
    type: REMOVE_TODO,
    payload:{
        index:id
    }
})